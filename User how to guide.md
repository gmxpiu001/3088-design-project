## How To Use The Gas Sensor

### Installing
1. Place you gas sensor in an appropriate space. This can either be on a piece of furniture such as stove , fridge cupboard etc.
> You should place the gas detector in an area that allows for free flow of air from the surroundings.
2. Plug the Gas sensor to a mains power socket using the power adapter.
3. The gas sensor will automatically go on when powered.

### Modes of Operation
The gas sensor has 2 major modes of operation.

#### 1.1 Test Mode
Use this mode to test that the gas detector can detect and present an alarm. By holding down the push button labelled `tst` the smoke detector will simulate gas present above the set threshold. 
#### 1.2 Active Mode
This is the default mode of the gas detector. In this mode, the gas detector will be actively sensing the current state of gas in the environment and when the threshold is met, present an alarm to you the user. To read up on the alarms, head over to the _Alarms Section_
#### 1.3 Reserve Power Mode
This mode is active whenever the gas detector is not being powered through the mains power. This would ideally happen in the event of a power blackout or when the switch through which the power adapter is connected, is off. In this mode, the gas detector will still sample the environment for a gas leakage but will try and conserve power usage. 
  
### Alarms and LED Outputs
The device will present an alarm 
* whenever it's triggered by detecting gas levels above the normal threshold (long beep through the speaker and red LED being illuminated)
* whenever it is not getting power through the mains supply (blinking yellow every 10s)

>The LEDs have been described further in the [LEDs description document](https://gitlab.com/gmxpiu001/3088-design-project/-/blob/63fd126e2a88f9278867fb0626eb63d9f64b4c0f/Subsystem%20Documentation/description.md)

>IMPORTANT!
> If the hat sounds the alarm it is likely you are in the presence of some sort of toxic gas and you should imediately vacate the area or find the source of the gas.
