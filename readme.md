## EEE3088F Project PCB Design

This is a git repo containing contributions towards building a draft PCB component for our Gas sensor PiHat. 

It is currently maintained by
- Zac Rubin 
- Pius Gumo

### Project Description
Our project is a low cost gas sensor that can be used in a household and would be able to alert users with regards to any gas leaks / dangerous situations as a result of excess gas presence in the environment.

### Navigating around
1. [How to use the Gas Detector and Getting Started](https://gitlab.com/gmxpiu001/3088-design-project/-/blob/main/User%20how%20to%20guide.md)
2. [What do the LEDs mean on your gas detector](https://gitlab.com/gmxpiu001/3088-design-project/-/blob/main/Subsystem%20Documentation/description.md)
3. [Manufacturing Your Gas Detector](https://gitlab.com/gmxpiu001/3088-design-project/-/blob/main/Manufacuring%20guide.md)
