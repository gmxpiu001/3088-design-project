## High Level

| Component | Function |
|--|--|
| LED_1 (Red)	| Indicates the gas detected alarm has been triggered. |
| LED_2	(Yellow)| Indicates the gas detector is being powered through the mains / through battery |
| LED_3	(Yellow)| Indicates the testing mode is currently active |
| PUSH_BUTTON_1 |	Used to reset the smoke detector alarm |
| PUSH_BUTTON_2 |	Used to test the gas detector |
| PI_ZERO |	Used to program the smoke detector |
| VOLTAGE_REGULATOR | Used to regulate the voltage of the pi zero such that the pi is powered within the correct current and voltage provisions|
