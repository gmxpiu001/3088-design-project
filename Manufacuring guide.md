## Manufacturing your own gas detector

To manufacture the PiHat, the following steps must be followed.

1. A PCB will need to be constructed using the [gerber files supplied](https://gitlab.com/gmxpiu001/3088-design-project/-/tree/main/Gerber). This can be done by supplying any PCB manufacturer with the gerber files and requesting however many PCB's you need.
2. If you want the PCB supplier to solder the parts for you, this can be chosen when ordering your PCB however if you want to do it yourself, the following must be done.
    - **2.1** To solder the parts yourself, you will need to have all the components included in the [bill of materials](https://gitlab.com/gmxpiu001/3088-design-project/-/blob/main/Assembly/PiHatBOM.csv) and you will need to have received printed PCB's from a manufacturer.
    - **2.2** You must then solder the parts carefully, in order to not damage any of the parts which may become damaged at high temperatures.
    - **2.3** Once the PCB is ready to be mounted, make use of the mounting holes to attach it to the Pi Zero with screws.

Once your smoke detector is created, you may go ahead and [start using it immeadiately!](https://gitlab.com/gmxpiu001/3088-design-project/-/blob/main/User%20how%20to%20guide.md)
