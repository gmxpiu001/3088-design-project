### How To Contribute
1. Clone the repo. `git clone gmxpiu001/pcb-design-project`
2. Create a feature branch `git checkout -b {feature-branch-name}`
3. Make contributions in your feature branch.
4. Commit the changed in your feature branch `git add . && git commit -m`
5. Create a Pull Request for your feature branch against this master branch.