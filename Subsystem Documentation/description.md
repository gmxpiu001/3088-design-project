## LEDS

### Introduction
The gas detector system uses LEDs as a way to communicate different messages to the user. 

There are currently 3 LEDs 
1. Gas Detector LED (Red)
1. Mode LED (Orange / Yellow)
1. Power LED (Orange / Yellow)

### Gas Detector LED
This LED is used to indicate the existence of Gas above the set threshold.
The LED will illuminate red whenever the gas sensor is triggered.

### Mode LED
This LED is used to indicate the current mode of the gas detector.
 - The led will illuminate a solid yellow light when in test mode. This is by physically pushing the test push button on the device.

### Power LED
 - The led will blink once every 10 seconds to indicate use in reserve power mode (i.e when it is being powered by the battery rather than the mains)
 - The led will remain off during a normal mode.
